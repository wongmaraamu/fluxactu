<?php
$sql="INSERT INTO `saves`(`title`, `link`, `derscription`) VALUES ([value-2],[value-3],[value-4])";
$exist="con";
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<title>FluxActu</title>
	<meta charset="utf-8">
	<link rel="shortcut icon" href="favicon.png" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.7/dist/css/uikit.min.css" />
	<script src="https://cdn.jsdelivr.net/npm/uikit@3.5.7/dist/js/uikit.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/uikit@3.5.7/dist/js/uikit-icons.min.js"></script>
	<link rel="stylesheet" href="style.css">
</head>

<!-- <header>
  <div class="container">
    <h1><a href="/"></a></h1>
    <nav>
      <a href="#"></a>
      <a href="#"></a>
      <a href="#"></a>
    </nav>
  </div>
</header> -->
<div class="container">
	<body>
		<!-- <button onclick="switchMode()">Mode Sombre</button> -->
		<div class="uk-container uk-margin-medium-top">
			<div class="uk-card uk-card-secondary uk-card-body">
				<h1 class="uk-heading-line uk-text-center"><span>FluxActu</span></h1>
				<form action=" " method="POST">
					<p>Sélectionner un flux : </p>
					<select class="uk-select" name="rss">
						<?php
						$feed_list = fopen("feeds.txt", "r");
						while (!feof($feed_list)) {
							$feed = str_replace(array("\n", "\r"), '', fgets($feed_list));
							echo "<option value='$feed'>$feed</option>";
						}
						fclose($feed_list);
						?>
					</select>
					<input type="hidden" name="show" value="1">
					<input class="uk-button uk-button-secondary uk-margin-top" type="submit" value="Afficher flux" >
					<a class="uk-button uk-button-default uk-margin-top" href="edit.php">Ajouter/Editer</a>
				</form>
			</div>
				<?php
				if (isset($_POST['show'])) {
					$rss = simplexml_load_file($_POST['rss']);
					echo '<h3>' . $rss->channel->title . '</h3>';
					foreach ($rss->channel->item as $item) {
						echo "<div class='uk-container uk-margin-small-top'>";
						echo "<div class='uk-card uk-card-default uk-card-body'>";
						echo '<h3><a href="' . $item->link . '" target="_blank">' . $item->title . "</a></h3>";
						echo "<p>" . $item->description . "</p>";
						if($item->img) echo "<img src=" . $item->img . " alt=''>";
						else if($item->image) echo "<img src=" . $item->image . " alt=''>";
						else if($item->picture) echo "<img src=" . $item->picture . " alt=''>";
						else if($item->link) echo "<img src=" . $item->link . " alt=''>";
						echo "</div>";
					}
				}
				?>
		</div>
		<script src="script.js"></script>
	</body>
</div>


</html>